# -*- coding: utf-8 -*-

from odoo import models, fields, api
import urllib, json
import logging

_logger = logging.getLogger(__name__)


class RickAndMortyApi(models.Model):
    _name = 'rick_and_morty_api'
    _description = 'Allows to consume information from rick and morty apy'

    search_name = fields.Char('Name', help='Name of the character that you will search')
    character_name = fields.Char('Character Name')
    character_status = fields.Char('Character Status')

    @api.multi
    def write_api_record(self):
        url = 'https://rickandmortyapi.com/api/character'
        db = 'rick_and_morty_apy'
        username = 'admin'
        password = 'admin'
        response = urllib.urlopen(url)
        data = json.loads(response.read())
        for record in data:
            _logger.info('This are the records {}'.format(record))
